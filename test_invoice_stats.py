import pytest
from invoice_stats import InvoiceStats


def test_assertTrue():
    assert True


def test_can_instantiate_InvoiceStats():
    invoice_data = InvoiceStats()


@pytest.fixture()
def invoice_data():
    invoice_data = InvoiceStats()
    return invoice_data


def test_can_add_invoice(invoice_data):
    invoice_data.add_invoice("ref#123", 1.20)


def test_can_add_invoice_list(invoice_data):
    invoice_data.add_invoices(["ref#4", "ref#5", "ref#6"], [7.55, 8.99, 9.15])
    assert invoice_data.amounts == [7.55, 8.99, 9.15]


def test_can_store_amounts_in_list(invoice_data):
    add_invoices(invoice_data)
    assert invoice_data.amounts == [1.20, 2.50, 3.80, 7.55, 8.99, 9.15]


def test_can_remove_all_stored_invoice_data(invoice_data):
    add_invoices(invoice_data)
    invoice_data.clear_data()
    assert invoice_data.data == {}


def test_can_calculate_median(invoice_data):
    add_invoices_for_stats(invoice_data)
    assert invoice_data.median() == 3


def test_can_calculate_mean(invoice_data):
    add_invoices_for_stats(invoice_data)
    assert invoice_data.mean() == 3


def add_invoices(invoice_data):
    invoice_data.add_invoice("ref#1", 1.20)
    invoice_data.add_invoice("ref#2", 2.50)
    invoice_data.add_invoice("ref#3", 3.80)
    invoice_data.add_invoices(["ref#4", "ref#5", "ref#6"], [7.55, 8.99, 9.15])


def add_invoices_for_stats(invoice_data):
    invoice_data.add_invoice("ref#A", 2.00)
    invoice_data.add_invoice("ref#B", 5.00)
    invoice_data.add_invoice("ref#C", 1.00)
    invoice_data.add_invoices(["ref#D", "ref#E"], [4.00, 3.00])


def test_add_invalid_string_format(invoice_data):
    with pytest.raises(ValueError) as e:
        invoice_data.add_invoice("ref#1", "$200,000,000")
    assert str(e.value) == "$200,000,000 is not an acceptable format"


def test_add_invalid_random_format(invoice_data):
    with pytest.raises(ValueError) as e:
        invoice_data.add_invoice("ref#1", "random")
    assert str(e.value) == "random is not an acceptable format"


def test_can_add_invoice_list_mix_formatting(invoice_data):
    invoice_data.add_invoices(["ref#8", "ref#9", "ref#10"], ["7.565", "8", 9.215])
    assert invoice_data.amounts == [7.57, 8.0, 9.21]


def test_under_min_invoice_value(invoice_data):
    with pytest.raises(ValueError):
        invoice_data.add_invoice("ref#ABC", -0.75)


def test_exceed_max_invoice_value(invoice_data):
    with pytest.raises(ValueError):
        invoice_data.add_invoice("ref#CDE", 200_000_001)

def test_exceed_max_number_invoices(invoice_data):
    with pytest.raises(Exception) as e:
        for i in range(1, 20_000_002):
            invoice_data.add_invoice(f"ref#{i}", i)
    assert str(e.value) == "Sorry max number of invoices reached: 20000000!"
