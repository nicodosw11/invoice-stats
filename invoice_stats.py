from utils import validate_invoice_value

class InvoiceStats:
    def __init__(self):
        self.data = {}
        self._amounts = []

    def add_invoice(self, reference, amount):
        """add a single invoice, in dollars and cents

        Arguments:
            reference {[string]} -- invoice reference
            amount {[number] [string]} -- invoice amount
            "1.01" with digits only or 1.01 or 1.012 are accepted; rounded to 2 decimal places.
        """
        if len(self.amounts) >= 20_000_000:
            raise Exception(f"Sorry max number of invoices reached: {len(self.amounts)}!")
        value = validate_invoice_value(amount)
        self.data[reference] = value
        self.amounts.append(value)

    @property
    def amounts(self):
        return self._amounts

    def add_invoices(self, invoice_references, invoice_values):
        """add a list of invoices, in dollars and cents

        Arguments:
            invoice_references {[list]} -- list of invoice references
            invoice_values {[list]} -- list of respective invoice amounts
        """
        for reference, amount in zip(invoice_references, invoice_values):
            self.add_invoice(reference, amount)

    def clear_data(self):
        self.data.clear()

    def median(self):
        l = list(self.amounts)
        n = len(l)
        l.sort()
        if n % 2 == 0:
            median1 = l[n // 2]
            median2 = l[n // 2 - 1]
            median = (median1 + median2) / 2
        else:
            median = l[n // 2]
        return median

    def mean(self):
        l = self.amounts
        n = len(l)
        get_sum = sum(l)
        mean = get_sum / n
        return mean
