def validate_invoice_value(value):
    try:
        value = "{:.2f}".format(float(value))
    except ValueError:
        raise ValueError(f"{value} is not an acceptable format")

    if float(value) < 0:
        raise ValueError(f"{value} cannot be less than 0")

    if float(value) > 200_000_000.00:
        raise ValueError(f"{value} cannot be greater than 200_000_000.00")

    return float(value)

